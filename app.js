const express = require("express");
const authRoutes = require("./src/routes/authentication.routes");
const studenthomeRoutes = require("./src/routes/studenthome.routes");
const userRoutes = require("./src/routes/user.routes");
require("dotenv").config();
const config = require("./src/config/config");
const logger = config.logger;

const app = express();
app.use(express.json());

app.use(function (req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE, OPTIONS");
	res.setHeader("Access-Control-Allow-Headers", "X-Requested-With,content-type, Authorization");
	res.setHeader("Access-Control-Allow-Credentials", true);
	next();
});

app.use("/api/users", userRoutes);
app.use("/api/studenthomes", studenthomeRoutes);
app.use("/api", authRoutes);

app.all("*", function (req, res) {
	logger.info("Non existing endpoint called");
	res.status(404).json({
		error: "Endpoint does not exist",
	});
});



module.exports = app;
