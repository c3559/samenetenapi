const config = require("./src/config/config");
const mongoose = require("mongoose");
require("dotenv").config();
const logger = config.logger;
const app = require("./app");

const host = "0.0.0.0";
const port = process.env.PORT || 3000;

mongoose.connect(
	`${process.env.MONGODB_CONNECTIONPROD_URL}?retryWrites=true&w=majority`,
	{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
).then(() => {
	app.listen(port, host, () => {
		logger.info(`server is listening on port ${port}`);
	});
});

