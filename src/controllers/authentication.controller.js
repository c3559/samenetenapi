/* eslint-disable no-unused-vars */
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const config = require("../config/config");
const logger = config.logger;
const neo4j = require("neo4j-driver");
const driver = neo4j.driver(process.env.NEO4J_CONNECTION_URL, neo4j.auth.basic("neo4j", process.env.NEO4J_CONNECTION_PASSWORD));

const User = require("../models/userSchema");

let controller = {
	signToken(user) {
		logger.info("signing token");
		
		const token = jwt.sign({
			id: user._id,
			firstName: user.firstName,
			lastName: user.lastName,
			email: user.email,
			studentNumber: user.studentNumber
		}, process.env.JWT_SECRET, { expiresIn: "1d" });
		return token;		
	},

	getInfo(_, res) {
		logger.info("Get called on /api/info");
		res.status(200).json({
			Name: "Lars Jongenelen",
			Studentnumber: 2156837,
			Description: "This is a server to promote social connections between people. Its a small scale project for students. Students can choose a house to host/join where they can eat with other students.",
			SonarQube: "https://sonarqube.avans-informatica-breda.nl/dashboard?id=node-prog"
		});
	},

	register(req, res) {
		logger.info("Post called on /api/register");
		let user = req.body;
		const saltrounds = parseInt(process.env.SALTROUNDS);
		const session = driver.session();
		let token = "";
		
		if (user.password){
			if (user.password.length < 8){
				res.status(400).json({
					error: "Password must have atleast 8 characters"
				});
			} else {
				bcrypt.hash(user.password, saltrounds).then(hash => {
					user.password = hash;
					return User.create(user);
				}).then(userReturned => {
					user = userReturned;
					token = controller.signToken(user);
					return session.run(`CREATE (user:User {_id: "${user._id}", firstName: "${user.firstName}", lastName: "${user.lastName}", email: "${user.email}"})`);	
					
				}).then(result => {
					session.close();
					res.status(200).json({
						firstName: user.firstName,
						lastName: user.lastName,
						authToken: token,
						_id: user._id
					});
				}).catch(err => {
					res.status(400).json({
						where: "REGISTER",
						error: err.message
					});
				});
			}

		} else {
			res.status(400).json({
				error: "User password is required"
			});
		}

	},
	
	login(req, res) {
		logger.info("Post called on /api/login");
		const email = req.body.email;
		const password = req.body.password;
		if (!email) {
			res.status(400).json({
				error: "Missing email"
			});
		} else if(!password) {
			res.status(400).json({
				error: "Missing password"
			});
		} else {
			User.findOne( { email }, { _id: 1, firstName: 1, lastName: 1, email: 1, studentNumber: 1, password: 1 } ).then(
				(user) => {
					if (user != null) {
						return bcrypt.compare(password, user.password).then(
							(comparison) => {
								if (comparison) {
									const token = controller.signToken(user);
									res.status(200).json({
										_id: user._id,
										firstName: user.firstName,
										lastName: user.lastName,
										authToken: token
									});
								} else {
									res.status(400).json({
										error: "Invalid password"
									});
								}
							}
						);
					} else {
						res.status(400).json({
							error: "User not found"
						});
					}
				}).catch(err => {
				res.status(400).json({
					error: err.message
				});
			});
		}
		
	},

	
	
};

module.exports = controller;
