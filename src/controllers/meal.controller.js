const config = require("../config/config");
const Studenthome = require("../models/studenthomeSchema");
const logger = config.logger;

let controller = {
	addMeal(req, res) {
		logger.info("Post called on /studenthome/:homeId/meal");
		let { user, studenthome, dateOffered, ...meal } = req.body;
		
		if (user.id == studenthome.maker._id) {
			meal.maker = user.id;
			let offeredOnDate = new Date(dateOffered);
			if (offeredOnDate ==  "Invalid Date") {
				res.status(400).json({
					error: "Invalid Date"
				});
			} else {
				meal.dateOffered = offeredOnDate;
				studenthome.meals.push(meal);
				studenthome.save({ validateBeforeSave: true })
					.then(studenthome => {
						const meal = studenthome.meals[studenthome.meals.length - 1];
						res.status(200).json({
							meal
						});
					}).catch(err => {
						res.status(400).json({
							error: err.message
						});
					});
			}
		} else {
			res.status(401).json({
				error: "You can only add meals to your own studenthome"
			});
		}
	},

	deleteMeal(req, res) {
		logger.info("Delete called on /studenthome/:homeId/meal/:mealId");
		const mealId = req.params.mealId;
		const studenthome = req.body.studenthome;
		const user = req.body.user;
		const meal = studenthome.meals.id(mealId);
		
		if (meal) {
			if(user.id == meal.maker._id) {
				meal.remove();
				studenthome.save({ validateBeforeSave: true }).then(() => {
					res.status(200).json({
						deleted: "Success"
					});
				});
			} else {
				res.status(401).json({
					error: "You can only delete your own meals"
				});
			}

		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},

	getAllMeals(req, res) {
		logger.info("Get called on /studenthomes/:homeId/meals");
		const studenthome = req.body.studenthome;
		Studenthome.findOne({ _id: studenthome._id }).then(home => {
			res.status(200).json({
				meals: home.meals
			});
		});
	},

	getMealInfoById(req, res) {
		logger.info("Get called on /studenthome/:homeId/meal/:mealId");
		const mealId = req.params.mealId;
		const studenthome = req.body.studenthome;
		const meal = studenthome.meals.id(mealId);
		if (meal) {
			res.status(200).json({
				meal: meal
			});
		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},

	updateMeal(req, res) {
		logger.info("Put called on /studenthome/:homeId/meal/:mealId");
		const mealId = req.params.mealId;
		let { user, studenthome, ...mealUpdated } = req.body;
		const mealUpdating = studenthome.meals.id(mealId);

		if (mealUpdating) {
			if(mealUpdating.maker._id == user.id) {
				Object.assign(mealUpdating, mealUpdated);
				Studenthome.findOne({ _id: studenthome._id, "meals._id": mealId, "meals.maker": user.id }).then(home => {
					if (home) {
						for (let i = 0; i < home.meals.length; i++) {
							let meal = home.meals[i];
							if (meal._id.toString() == mealId) {
								home.meals[i] = mealUpdating;
							}
						}
						Studenthome.findOneAndUpdate({ _id: studenthome._id, "meals._id": mealId, "meals.maker": user.id },
							home,
							{ upsert: false, new: true, runValidators: true }).then(updatedHome => {
							res.status(200).json({
								updatedHome
							});
						}).catch(err => {
							res.status(400).json({
								error: err.message
							});
						});
					}  else {
						res.status(400).json({
							error: "Studenthome not found"
						});
					}
				});

			} else {
				res.status(401).json({
					error: "You can only edit your own meals"
				});
			}

		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},
};

module.exports = controller;
