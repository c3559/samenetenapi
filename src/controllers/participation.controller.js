const config = require("../config/config");
const Studenthome = require("../models/studenthomeSchema");
const logger = config.logger;

let controller = {
	signUp(req, res) {
		logger.info("Post called on /studenthome/:homeId/meal/:mealId/signup");
		let mealId = req.params.mealId;
		let user = req.body.user;
		let studenthome = req.body.studenthome;
		let meal = studenthome.meals.id(mealId);

		if (meal) {
			if (meal.participants.length < meal.maxParticipants) {
				meal.participants.push({ user: user.id });
				let participantId = studenthome.meals.id(mealId).participants.length - 1;
				studenthome.save({ validateBeforeSave: true }).then(home => {
					let participant = home.meals.id(mealId).participants[participantId];
					res.status(200).json({
						"participant": participant
					}); 
				}).catch(err => {
					res.status(400).json({
						error: err.message
					});
				});
			} else {
				res.status(400).json({
					error: "Meal already has the max amount of participants"
				});
			}
		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},

	signOff(req, res) {
		logger.info("Put called on /studenthome/:homeId/meal/mealId/signoff");
		let mealId = req.params.mealId;
		let user = req.body.user;
		let studenthome = req.body.studenthome;
		let meal = studenthome.meals.id(mealId);
		let foundParticipant = false;
		let returnParticipant;

		if (meal) {
			Studenthome.findOne({ _id: studenthome._id, "meals._id": mealId }).then(home => {
				if (home) {
					for (let i = 0; i < meal.participants.length; i++) {
						let participant = meal.participants[i];
						if (participant.user == user.id) {
							returnParticipant = participant;
							home.meals.id(mealId).participants.pull({ _id: participant._id });
							foundParticipant = true;
						}
					}
					logger.info(home.meals.id(mealId).participants);
					if (foundParticipant) {
						Studenthome.findOneAndUpdate({ _id: studenthome._id, "meals._id": mealId },
							home,
							{ upsert: false, new: true, runValidators: true }).then(() => {
							res.status(200).json({
								participant: returnParticipant
							});
						}).catch(err => {
							res.status(400).json({
								error: err.message
							});
						});
					} else {
						res.status(404).json({
							error: "Participant not found"
						});
					}
					
				}  else {
					res.status(404).json({
						error: "Studenthome not found"
					});
				}
			}).catch(err => {
				res.status(400).json({
					error: err.message
				});
			});
		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},

	getAllParticipants(req, res) {
		logger.info("Get called on /studenthome/:homeId/meal/mealId/participants");
		let mealId = req.params.mealId;

		Studenthome.findOne({ "meals._id": mealId }).populate("meals.participants.user").then(home => {
			if(home) {
				res.status(200).json({
					participants: home.meals.id(mealId).participants
				});
			} else {
				res.status(404).json({
					error: "Meal not found"
				});
			}
			
		}).catch(err => {
			res.status(400).json({
				error: err.message
			});
		});

	},

	getParticipantInfoById(req, res) {
		logger.info("Get called on /studenthome/:homeId/meal/mealId/participants/:participantId");
		let mealId = req.params.mealId;
		let participantId = req.params.participantId;
		
		Studenthome.findOne({ "meals._id": mealId }).populate("meals.participants.user").then(home => {
			if(home) {
				const participant= home.meals.id(mealId).participants.id(participantId);
				if (participant != null) {
					res.status(200).json({
						participant
					});
				} else {
					res.status(404).json({ 
						error: "Participant not found"
					});
				}
				
			} else {
				res.status(404).json({
					error: "Meal not found"
				});
			}
			
		}).catch(err => {
			res.status(400).json({
				error: err.message
			});
		});
	},
};

module.exports = controller;
