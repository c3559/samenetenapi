const config = require("../config/config");
const Studenthome = require("../models/studenthomeSchema");
const logger = config.logger;

let controller = {
	addReview(req, res) {
		logger.info("Post called on /studenthome/:homeId/meals/:mealId/reviews");
		let { user, studenthome, ...review } = req.body;
		const mealId = req.params.mealId;
		const meal = studenthome.meals.id(mealId);
		review.maker = user.id;
		if (meal) {
			meal.reviews.push(review);
			studenthome.save({ validateBeforeSave: true })
				.then(studenthome => {
					const reviews = studenthome.meals.id(mealId).reviews;
					const review = reviews[reviews.length - 1];
					res.status(200).json({
						review
					});
				}).catch(err => {
					res.status(400).json({
						error: err.message
					});
				});
		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},

	deleteReview(req, res) {
		logger.info("Delete called on /studenthome/:homeId/meals/:mealId/reviews/:reviewId");
		const mealId = req.params.mealId;
		const studenthome = req.body.studenthome;
		const user = req.body.user;
		const meal = studenthome.meals.id(mealId);
		if (meal) {
			const reviewId = req.params.reviewId;
			const review = meal.reviews.id(reviewId);
			if (review) {
				if(user.id == review.maker) {
					review.remove();
					studenthome.save({ validateBeforeSave: true }).then(() => {
						res.status(200).json({
							deleted: "Success"
						});
					});
				} else {
					res.status(401).json({
						error: "You can only delete your own reviews"
					});
				}

			} else {
				res.status(404).json({
					error: "Review not found"
				});
			}
		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},

	getAllReviews(req, res) {
		logger.info("Get called on /studenthomes/:homeId/meals/:mealId/reviews");
		let mealId = req.params.mealId;

		Studenthome.findOne({ "meals._id": mealId }).populate("meals.reviews.maker").then(home => {
			if(home) {
				res.status(200).json({
					reviews: home.meals.id(mealId).reviews
				});
			} else {
				res.status(404).json({
					error: "Meal not found"
				});
			}
			
		}).catch(err => {
			res.status(400).json({
				error: err.message
			});
		});
	},

	getReviewInfoById(req, res) {
		logger.info("Get called on /studenthome/:homeId/meals/:mealId/reviews/:reviewId");
		let mealId = req.params.mealId;
		let reviewId = req.params.reviewId;
		
		Studenthome.findOne({ "meals._id": mealId }).populate("meals.reviews.maker").then(home => {
			if(home) {
				const review= home.meals.id(mealId).reviews.id(reviewId);
				if (review != null) {
					res.status(200).json({
						review
					});
				} else {
					res.status(404).json({ 
						error: "Review not found"
					});
				}
				
			} else {
				res.status(404).json({
					error: "Meal not found"
				});
			}
			
		}).catch(err => {
			res.status(400).json({
				error: err.message
			});
		});
	},

	updateReview(req, res) {
		logger.info("Put called on /studenthome/:homeId/meals/:mealId/reviews/:reviewId");
		const mealId = req.params.mealId;
		let { user, studenthome, ...reviewUpdated } = req.body;
		const meal = studenthome.meals.id(mealId);
		const reviewId = req.params.reviewId;

		if (meal) {
			let reviewUpdating = meal.reviews.id(reviewId);
			if(reviewUpdating) {
				if(reviewUpdating.maker == user.id) {
					Object.assign(reviewUpdating, reviewUpdated);
					Studenthome.findOne({ _id: studenthome._id, "meals._id": mealId }).then(home => {
						if (home) {
							let meal = home.meals.id(mealId);
							for (let i = 0; i < meal.reviews.length; i++) {
								let review = meal.reviews[i];
								if (review._id.toString() == reviewId) {
									meal.reviews[i] = reviewUpdating;
								}
							}
							Studenthome.findOneAndUpdate({ _id: studenthome._id, "meals._id": mealId },
								home,
								{ upsert: false, new: true, runValidators: true }).then(updatedHome => {
								let updatedMeal = updatedHome.meals.id(mealId);
								res.status(200).json({
									updatedMeal
								});
							}).catch(err => {
								res.status(400).json({
									error: err.message
								});
							});
						}  else {
							res.status(400).json({
								error: "Studenthome not found"
							});
						}
					});
				} else {
					res.status(401).json({
						error: "You can only edit your own reviews"
					});
				}

			} else {
				res.status(404).json({
					error: "Review not found"
				});
			}

		} else {
			res.status(404).json({
				error: "Meal not found"
			});
		}
	},
};

module.exports = controller;
