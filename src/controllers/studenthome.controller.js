const config = require("../config/config");
const logger = config.logger;

const Studenthome = require("../models/studenthomeSchema");

let controller = {

	createStudenthome(req, res) {
		logger.info("Post called on /api/studenthome");
		let { user, ...studenthome } = req.body;
		let home = { ...studenthome };
		home.maker = user.id;
		Studenthome.create(home).then(home => {
			if (home != null) {
				res.status(200).json({ home });
			}
		}).catch(err => {
			res.status(400).json({ error: err.message });
		});
	},

	deleteStudenthome(req, res) {
		logger.info("Delete called on /api/studenthome/:homeId");
		let user = req.body.user;
		const id = req.params.homeId;
		Studenthome.findOne({ _id: id }).then(home => {
			if (home) {
				if (home.maker == user.id) {
					Studenthome.findOneAndDelete({ _id: id })
						.then(() => {
							res.status(200).json({
								Delete: "Success"
							});
						});
				} else {
					res.status(401).json({
						error: "You can only delete your own Studenthome"
					});
				}
			} else {
				res.status(404).json({
					error: "Studenthome not found"
				});
			}
		}).catch(() => {
			res.status(400).json({
				error: "Invalid ID"
			});
		});		
	},

	getByNameAndCity(req, res) {
		logger.info("Get called on /api/studenthome?name=:name&city=:city");
		const name = req.query.name;
		const city = req.query.city;
		let filter = {};
		if (name) {
			filter.name = new RegExp(`^${name}`);
		}
		if (city) {
			filter.city = city;
		}
		Studenthome.find(filter).then(studenthomes => {
			if (studenthomes.length) {
				res.status(200).json({
					studenthomes
				});
			} else {
				res.status(404).json({ 
					error: "No studenthomes found"
				});
			}
		});
        
	},

	getStudenthome(req, res) {
		logger.info("Get called on /studenthome/:homeId");
		const id = req.params.homeId;		
		Studenthome.findOne({ _id: id }).populate("maker").then(studenthome => {
			if (studenthome) {
				res.status(200).json({
					studenthome
				});
			} else {
				res.status(404).json({
					error: "Studenthome not found"
				});
			}
		}).catch(() => {
			res.status(400).json({
				error: "Invalid ID"
			});
		});	
		
	},

	updateStudenthome(req, res) {
		logger.info("Put called on /studenthome/:homeId");
		let { user, ...studenthome } = req.body;
		const id = req.params.homeId;
		Studenthome.findOne({ _id: id }).then(home => {
			if (home) {
				if (home.maker == user.id) {
					Studenthome.findOneAndUpdate({ _id: id }, studenthome, { upsert: false, new: true, runValidators: true })
						.then(updatedHome => {
							res.status(200).json({
								updatedHome
							});
						}).catch(err => {
							res.status(400).json({
								error: err.message
							});
						});
				} else {
					res.status(400).json({
						error: "You can only edit your own Studenthome"
					});
				}
			} else {
				res.status(400).json({
					error: "Studenthome not found"
				});
			}
		}).catch(() => {
			res.status(400).json({
				error: "Invalid ID"
			});
		});
		
	},
};

module.exports = controller;
