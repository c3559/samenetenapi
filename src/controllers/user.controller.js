/* eslint-disable no-unused-vars */
require("dotenv").config();
const config = require("../config/config");
const logger = config.logger;
const neo4j = require("neo4j-driver");
const driver = neo4j.driver(process.env.NEO4J_CONNECTION_URL, neo4j.auth.basic("neo4j", process.env.NEO4J_CONNECTION_PASSWORD));
const User = require("../models/userSchema");
const Studenthome = require("../models/studenthomeSchema");
const mongoose = require("mongoose");

let controller = {

	follow(req, res) {
		logger.info("Post called /users/:userId/follow");
		const session = driver.session();
		const id = req.params.userId;
		let { user } = req.body;
		const currentUser  = user.id;
		User.findOne({ _id: id }).then(user => {
			if(user) {
				return session.run(`MATCH (a:User) WHERE a._id = "${currentUser}" MATCH (b:User) WHERE b._id = "${id}" CREATE (a)-[r:FOLLOWS]->(b)`);
			} else {
				res.status(404).json({
					error: "User not found"
				});
			}
		}).then(result => {
			session.close();
			res.status(200).json({
				success: "You are now following!"
			});
		}).catch(error => {
			res.status(400).json({
				error: error
			});
		});

	},

	unfollow(req, res) {
		logger.info("Post called /users/:userId/unfollow");
		const session = driver.session();
		const id = req.params.userId;
		let { user } = req.body;
		const currentUser  = user.id;
		User.findOne({ _id: id }).then(user => {
			if(user) {
				return session.run(`MATCH (a:User) WHERE a._id = "${currentUser}" MATCH (b:User) WHERE b._id = "${id}"  MATCH (a)-[r:FOLLOWS]->(b) DELETE r`);
			} else {
				res.status(404).json({
					error: "User not found"
				});
			}
		}).then(() => {
			session.close();
			res.status(200).json({
				success: "You have unfollowed!"
			});
		}).catch(error => {
			res.status(400).json({
				error: error
			});
		});
	},

	getUserById(req, res) {
		logger.info("Get called on /users/:userId");
		const id = req.params.userId;		
		User.findOne({ _id: id }).then(user => {
			if (user) {
				res.status(200).json({
					user
				});
			} else {
				res.status(404).json({
					error: "User not found"
				});
			}
		}).catch(error => {
			res.status(400).json({
				error: error
			});
		});	
	},

	getAllFollowed(req, res) {
		logger.info("Get called on /users/following");
		const session = driver.session();
		let { user } = req.body;
		const currentUser  = user.id;
		session.run(`MATCH (a:User) WHERE a._id = "${currentUser}" MATCH (a)-[r:FOLLOWS]->(b) RETURN b`).then(result => {
			let userArray = [];
			for (let i = 0; i < result.records.length; i++) {
				let record = result.records[i];
				for (let j = 0; j < record._fields.length; j++) {
					let user = record._fields[j].properties;
					userArray.push(user);
				}
			}
			return userArray;
		}).then(array => {
			session.close();
			res.status(200).json({
				users: array
			});
		}).catch(error => {
			res.status(400).json({
				error: error
			});
		});
	},

	getAllFollowedMeals(req, res) {
		logger.info("Get called on /users/following/meals");
		const session = driver.session();
		let { user } = req.body;
		const currentUser  = user.id;

		session.run(`MATCH (a:User) WHERE a._id = "${currentUser}" MATCH (a)-[r:FOLLOWS]->(b) RETURN b`).then(result => {
			let userArray = [];
			for (let i = 0; i < result.records.length; i++) {
				let record = result.records[i];
				for (let j = 0; j < record._fields.length; j++) {
					let user = record._fields[j].properties;
					userArray.push(user);
				}
			}
			return userArray;
		}).then(array => {
			session.close();
			let idArray = [];
			for (let i = 0; i < array.length; i++) {
				idArray.push(mongoose.Types.ObjectId(array[i]._id));
			}
			return Studenthome.find({ "meals.maker": { $in: idArray } });
		}).then(homes => {
			let mealArray = [];
			for (let i = 0; i < homes.length; i++) {
				for (let j = 0; j < homes[i].meals.length; j++) {
					if (homes[i].meals[j].participants.length < homes[i].meals[j].maxParticipants) {
						mealArray.push(homes[i].meals[j]);
					}
				}
			}
			res.status(200).json({ 
				meals: mealArray
			});
		}).catch(error => {
			res.status(400).json({
				error: error
			});
		});
	}
	
};

module.exports = controller;

