const jwt = require("jsonwebtoken");
const config = require("../config/config");
require("dotenv").config();

const logger = config.logger;
module.exports = (req, res, next) => {
	logger.info("checking authentication");
	const authHeader = req.headers.authorization;
	const token = authHeader && authHeader.split(" ")[1];
	if (token == undefined) {
		res.status(401).json({
			error: "Not authenticated"
		});
	} else {
		jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
			if (err) {
				res.status(401).json({
					error: "Not authenticated"
				});
			} else {
				req.body.user = user;
				next();
			}
		});
	}
};
