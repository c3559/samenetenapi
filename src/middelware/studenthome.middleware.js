const config = require("../config/config");
const logger = config.logger;
const Studenthome = require("../models/studenthomeSchema");

module.exports = (req, res, next) => {
	logger.info("checking studenthome");

	Studenthome.findById(req.params.homeId).populate("maker").populate("meals.maker")
		.then(studenthome => {
			if(studenthome){
				req.body.studenthome = studenthome;
				next();
			} else {
				res.status(400).json({
					error: "Studenthome not found"
				});
			}
		});
};
