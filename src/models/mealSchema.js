const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const participantSchema = require("./participantSchema");
const reviewSchema = require("./reviewSchema");

const mealSchema = new Schema({
	maker: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "Meal maker is required"]
	},
	name: {
		type: String,
		required: [true, "Meal name is required"]
	},
	description: {
		type: String,
		required: [true, "Meal description is required"]
	},
	dateCreated: {
		type: Date,
		default: new Date()
	},
	dateOffered: {
		type: Date,
		required: [true, "Meal offer date is required"]
	},
	price: {
		type: Number,
		min: [0, "Meal price can't be negative"],
		required: [true, "Meal price is required"]
	},
	allergyInfo: {
		type: String,
		required: [true, "Meal allergy information is required"]
	},
	ingredients: {
		type: String,
		required: [true, "Meal ingredients is required"]
	},
	maxParticipants: {
		type: Number,
		min: [1, "Meal maximum participants needs to be atleast 1"],
		default: 10
	},
	participants: {
		type: [participantSchema],
		default: []
	},
	reviews: {
		type: [reviewSchema],
		default: []
	}
});

module.exports = mealSchema;