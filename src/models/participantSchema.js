const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const participantSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "Participant user is required!"]
	},
	signupDate: {
		type: Date,
		default: new Date()
	}
});

module.exports = participantSchema;