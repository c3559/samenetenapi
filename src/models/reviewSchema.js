const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reviewSchema = new Schema({
	maker: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "Review maker is required!"]
	},
	title: {
		type: String,
		required: [true, "Review name is required"]
	},
	description: {
		type: String,
		required: [true, "Review description is required"]
	},
	rating: {
		type: Number,
		required: [true, "Review rating is required"],
		min: [0, "Review rating needs to be atleast 0"],
		max: [10, "Review rating can be 10 at most"],
	},
	dateCreated: {
		type: Date,
		default: new Date()
	},
	recommended: {
		type: Boolean,
		required: [true, "Review recommendation is required"]
	},
});

module.exports = reviewSchema;