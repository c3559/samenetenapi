const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const mealSchema = require("./mealSchema");

const studenthomeSchema = new Schema({
	maker: {
		type: Schema.Types.ObjectId,
		ref: "user",
		required: [true, "Studenthome maker is required"]
	},
	name: {
		type: String,
		required: [true, "Studenthome name is required"]
	},
	streetname: {
		type: String,
		required: [true, "Studenthome streetname is required"]
	},
	housenumber: {
		type: String,
		required: [true, "Studenthome housenumber is required"],
		index: true
	},
	postalcode: {
		type: String,
		required: [true, "Studenthome postalcode is required"],
		match: /^[1-9][0-9]{3}\s?[a-zA-Z]{2}$/,
		index: true
	},
	city: {
		type: String,
		required: [true, "Studenthome city is required"]
	},
	phonenumber: {
		type: String,
		required: [true, "Studenthome phonenumber required"],
		match: /^([0]{1}[6]{1}[-\s]*[1-9]{1}[\s]*([0-9]{1}[\s]*){7})|([0]{1}[1-9]{1}[0-9]{1}[0-9]{1}[-\s]*[1-9]{1}[\s]*([0-9]{1}[\s]*){5})|([0]{1}[1-9]{1}[0-9]{1}[-\s]*[1-9]{1}[\s]*([0-9]{1}[\s]*){6})$/
	},	
	meals: {
		type: [mealSchema],
		default: []
	}
});

studenthomeSchema.index({
	housenumber: 1,
	postalcode: 1
}, {
	unique: true
});


studenthomeSchema.post("save", function(error, doc, next) {
	if (error.name === "MongoError" && error.code === 11000) {
		next(new Error("Studenthome already exists"));
	} else {
		next(error);
	}
});


const Studenthome = mongoose.model("studenthomes", studenthomeSchema);

module.exports = Studenthome;