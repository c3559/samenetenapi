const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
	firstName: {
		type: String,
		required: [true, "User first name is required"]
	},
	lastName: {
		type: String,
		required: [true, "User last name is required"]
	},
	studentNumber: {
		type: String,
		required: [true, "User studentnumber is required"]
	},
	email: {
		type: String,
		required: [true, "User email is required"],
		match: [/^[^@\s]+@[^@\s]+\.[^@\s]+$/, "Invalid email"],
		unique: true
	},
	password: {
		type: String,
		required: [true, "User password is required"],
		select: false
	}
});

userSchema.post("save", function(error, doc, next) {
	if (error.name === "MongoError" && error.code === 11000) {
		next(new Error("Email already exists"));
	} else {
		next(error);
	}
});

const User = mongoose.model("user", userSchema);

module.exports = User;