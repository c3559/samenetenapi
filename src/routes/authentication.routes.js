const express = require("express");
const router = express.Router();
const authController = require("../controllers/authentication.controller");


router.get("/info", authController.getInfo);
router.post("/register", authController.register);
router.post("/login", authController.login);

module.exports = router;
