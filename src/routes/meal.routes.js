const express = require("express");
const router = express.Router();
const mealController = require("../controllers/meal.controller");
const authMiddleware = require("../middelware/authentication.middleware");
const participationController = require("../controllers/participation.controller");
const reviewController = require("../controllers/review.controller");

router.post("/", authMiddleware,  mealController.addMeal);
router.put("/:mealId", authMiddleware, mealController.updateMeal);
router.get("/", mealController.getAllMeals);
router.get("/:mealId", mealController.getMealInfoById);
router.delete("/:mealId", authMiddleware, mealController.deleteMeal);

router.post("/:mealId/signup", authMiddleware, participationController.signUp);
router.put("/:mealId/signoff", authMiddleware, participationController.signOff);
router.get("/:mealId/participants", authMiddleware, participationController.getAllParticipants);
router.get("/:mealId/participants/:participantId", authMiddleware, participationController.getParticipantInfoById);

router.post("/:mealId/reviews", authMiddleware, reviewController.addReview);
router.put("/:mealId/reviews/:reviewId", authMiddleware, reviewController.updateReview);
router.get("/:mealId/reviews", reviewController.getAllReviews);
router.get("/:mealId/reviews/:reviewId", reviewController.getReviewInfoById);
router.delete("/:mealId/reviews/:reviewId", authMiddleware, reviewController.deleteReview);

module.exports = router;