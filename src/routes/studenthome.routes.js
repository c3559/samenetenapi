const express = require("express");
const router = express.Router();
const studenthomeController = require("../controllers/studenthome.controller");
const authMiddleware = require("../middelware/authentication.middleware");
const homeMiddleware = require("../middelware/studenthome.middleware");
const mealRoutes = require("./meal.routes");

//Studenthome routes
router.get("/", studenthomeController.getByNameAndCity);
router.post("/", authMiddleware, studenthomeController.createStudenthome);
router.get("/:homeId", studenthomeController.getStudenthome);
router.delete("/:homeId", authMiddleware, studenthomeController.deleteStudenthome);
router.put("/:homeId", authMiddleware, studenthomeController.updateStudenthome);

router.use("/:homeId/meals", homeMiddleware, mealRoutes);


module.exports = router;
