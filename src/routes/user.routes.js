const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.controller");
const authMiddleware = require("../middelware/authentication.middleware");

//User routes
router.post("/:userId/unfollow", authMiddleware, userController.unfollow);
router.post("/:userId/follow", authMiddleware, userController.follow);
router.get("/following/meals", authMiddleware, userController.getAllFollowedMeals);
router.get("/following", authMiddleware, userController.getAllFollowed);
router.get("/:userId", userController.getUserById);


module.exports = router;
