/* eslint-disable no-undef */
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const app = require("../../app");
const User = require("../../src/models/userSchema");


const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
require("dotenv").config();



chai.should();
chai.use(chaiHttp);

const saltrounds = parseInt(process.env.SALTROUNDS);

describe("Manage authentication", () => {
	beforeEach(done => {
		User.create({
			firstName: "test",
			lastName: "test",
			studentNumber: "20984",
			email: "test@gmail.com",
			password: bcrypt.hashSync("testpassword", saltrounds)
		}).then(() =>  {
			done();
		});
	});
	describe("UC-101 Register - POST /api/register", () => {
		it("TC-101-1 Should return valid error when required field is not present", (done) => {
			chai.request(app)
				.post("/api/register")
				.send({
					firstName: "test",
					lastName: "test",
					//studentNumber: "20182",
					email: "test1@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "user validation failed: studentNumber: User studentnumber is required");
				
					done();
				});
		});

		it("TC-101-2 Should return valid error when email is invalid", (done) => {
			chai.request(app)
				.post("/api/register")
				.send({
					firstName: "test",
					lastName: "test",
					studentNumber: "20182",
					email: "testgmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "user validation failed: email: Invalid email");

					done();
				});
		});

		it("TC-101-3 Should return valid error when password is invalid", (done) => {
			chai.request(app)
				.post("/api/register")
				.send({
					firstName: "test",
					lastName: "test",
					studentNumber: "20182",
					email: "test1@gmail.com",
					password: "test"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Password must have atleast 8 characters");
					
					done();
				});
		});

		it("TC-101-4 Should return valid error when user already exists", (done) => {
			chai.request(app)
				.post("/api/register")
				.send({
					firstName: "test",
					lastName: "test",
					studentNumber: "20984",
					email: "test@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Email already exists");

					done();
				});
		});

		
		it("TC-101-5 Should succesfully register user when data is valid", (done) => {
			chai.request(app)
				.post("/api/register")
				.send({
					firstName: "neo4jtestuserfirstnamebeforeeach",
					lastName: "test",
					studentNumber: "20984",
					email: "testRegister@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {

					User.findOne({ email: "testRegister@gmail.com" }).then(user => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("authToken");
						const verifiedToken = jwt.verify(res.body.authToken, process.env.JWT_SECRET);
						expect(verifiedToken).to.haveOwnProperty("firstName", user.firstName);
						expect(verifiedToken).to.haveOwnProperty("id", user._id.toString());
						expect(res.body).to.haveOwnProperty("firstName", user.firstName);
						expect(res.body).to.haveOwnProperty("lastName", user.lastName);
						done();
					});	
					
				});
		});

	});

	
	describe("UC-102 Login - POST /api/login", () => {
		
		it("TC-102-1 Should return valid error when required field is not present", (done) => {
			chai.request(app)
				.post("/api/login")
				.send({
					//email: "test@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Missing email");

					done();
				});
		});

		it("TC-102-2 Should return valid error when email is invalid", (done) => {
			chai.request(app)
				.post("/api/login")
				.send({
					email: "testfalse@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "User not found");

					done();
				});
		});

		it("TC-102-3 Should return valid error when password is invalid", (done) => {
			chai.request(app)
				.post("/api/login")
				.send({
					email: "test@gmail.com",
					password: bcrypt.hashSync("testfalsepassword", 10)
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Invalid password");

					done();
				});
		});

		it("TC-102-4 Should return valid error when user does not exist", (done) => {
			chai.request(app)
				.post("/api/login")
				.send({
					email: "testnonexistant@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "User not found");

					done();
				});
		});
		it("TC-102-5 Should succesfully login user when data is valid", (done) => {
			
			chai.request(app)
				.post("/api/login")
				.send({
					email: "test@gmail.com",
					password: "testpassword"
				})
				.end((err, res) => {
					User.findOne({ email: "test@gmail.com" }).then(user => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("authToken");
						const verifiedToken = jwt.verify(res.body.authToken, process.env.JWT_SECRET);
						expect(verifiedToken).to.haveOwnProperty("firstName", user.firstName);
						expect(verifiedToken).to.haveOwnProperty("id", user._id.toString());
						expect(res.body).to.haveOwnProperty("firstName", user.firstName);
						expect(res.body).to.haveOwnProperty("lastName", user.lastName);
						done();
					});
				});	
					
		});
	});

});