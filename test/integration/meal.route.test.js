/* eslint-disable no-undef */
const app = require("../../app");

const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const bcrypt = require("bcrypt");
const saltrounds = parseInt(process.env.SALTROUNDS);

const Studenthome = require("../../src/models/studenthomeSchema");
const User = require("../../src/models/userSchema");

const authController = require("../../src/controllers/authentication.controller");

chai.should();
chai.use(chaiHttp);
	

//meal Tests
describe("Manage meals", () => {
	let beforeEachUser;
	let beforeEachMealId;
	let beforeEachHomeId;
	let authToken;
	let fakeToken;

	beforeEach(done => {
		User.create({
			firstName: "testFakeBefore",
			lastName: "testFakeBefore",
			studentNumber: "20984",
			email: "testFakeBefore@gmail.com",
			password: bcrypt.hashSync("testFakeBeforepassword", saltrounds)
		}).then(user => {
			fakeToken = authController.signToken(user);
			return User.create({
				firstName: "testBefore",
				lastName: "testBefore",
				studentNumber: "20984",
				email: "testBefore@gmail.com",
				password: bcrypt.hashSync("testBeforepassword", saltrounds)
			});
		}).then(user =>  {
			beforeEachUser = user;
			authToken = authController.signToken(user);

			return Studenthome.create({
				maker: beforeEachUser,
				name: "testBeforeHome",
				streetname: "testBeforeHomeStreet",
				housenumber: 1,
				postalcode: "4377 OP",
				city: "testBeforeHomeCity",
				phonenumber: "0612345678"
			});
		}).then(home => {
			beforeEachHomeId = home._id;
			home.meals.push({
				maker: beforeEachUser._id,
				name: "testBeforeMeal",
				description: "testMeal",
				dateOffered: new Date(2022, 5, 3),
				price: 10,
				allergyInfo: "Test",
				ingredients: "PizzaTest",
				maxParticipants: 4
			});
			return home.save({ validateBeforeSave: true });
		}).then(homeMeal => {
			beforeEachMealId = homeMeal.meals[0]._id;
			done();
		});
	});

	describe("UC301 Create meal - POST /api/meal", () => {
		it("TC-301-1 Should return valid error when required field is not present", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "test",
					//description: "testDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 20,
					allergyInfo: "testAllergies",
					ingredients: "testIngredients",
					maxParticipants: 3
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "studenthomes validation failed: meals.1.description: Meal description is required");
				
					done();
				});
		});

		it("TC-301-2 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals`)
				.send({
					name: "test",
					description: "testDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 20,
					allergyInfo: "testAllergies",
					ingredients: "testIngredients",
					maxParticipants: 3
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-301-3 Should return valid meal when correctly added", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "test",
					description: "testDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 20,
					allergyInfo: "testAllergies",
					ingredients: "testIngredients",
					maxParticipants: 3
				})
				.end((err, res) => {
					Studenthome.findById(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const meal = res.body.meal;
						const foundMeal = home.meals[1];
						expect(meal).to.haveOwnProperty("dateCreated").to.be.a("string");
						const mealDate = new Date(meal.dateCreated);
						expect(mealDate.toString()).to.equal(foundMeal.dateCreated.toString());
						expect(meal).to.haveOwnProperty("_id", foundMeal._id.toString());
						expect(meal).to.haveOwnProperty("name", foundMeal.name);
						expect(meal).to.haveOwnProperty("price", foundMeal.price);
						done();
					});
					
				});
		});

	});

	describe("UC302 Edit meal - PUT /api/meal/:id", () => {
		
		it("TC-302-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.send({
					name: "test",
					description: "testDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 20,
					allergyInfo: "testAllergies",
					ingredients: "testIngredients",
					maxParticipants: 3
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-302-2 Should return valid error when user is not owner of studenthome", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.set("Authorization", `Bearer ${fakeToken}`)
				.send({
					name: "test",
					description: "testDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 20,
					allergyInfo: "testAllergies",
					ingredients: "testIngredients",
					maxParticipants: 3
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "You can only edit your own meals");
				
					done();
				});
		});

		it("TC-302-3 Should return valid error when meal doesnt exist", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/60e1c8394b9af920c8ceb3e2`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "test",
					description: "testDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 20,
					allergyInfo: "testAllergies",
					ingredients: "testIngredients",
					maxParticipants: 3
				})
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		}); 

		it("TC-302-4 Should return valid meal when correctly edited", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "newtest",
					description: "newtestDescription",
					dateOffered: "2025-01-21T12:00:00Z",
					price: 200,
					allergyInfo: "newtestAllergies",
					ingredients: "newtestIngredients",
					maxParticipants: 3
				})
				.end((err, res) => {
					Studenthome.findById(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const studenthome = res.body.updatedHome;
						const meal = studenthome.meals[0];
						const foundMeal = home.meals[0];
						expect(meal).to.haveOwnProperty("dateCreated").to.be.a("string");
						const mealDate = new Date(meal.dateCreated);
						expect(mealDate.toString()).to.equal(foundMeal.dateCreated.toString());
						expect(meal).to.haveOwnProperty("_id", foundMeal._id.toString());
						expect(meal).to.haveOwnProperty("name", foundMeal.name);
						expect(meal).to.haveOwnProperty("price", foundMeal.price);
						done();
					});
				});
		});

	});


	describe("UC303 Overview of meals - GET /api/meal", () => {
		it("TC-303-1 Should return all meals", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					Studenthome.findOne(beforeEachHomeId).then(home => {
						foundMeals = home.meals;
						meal = res.body.meals[0];
						foundMeal = home.meals[0];
						expect(meal).to.haveOwnProperty("dateOffered").to.be.a("string");
						const mealDate = new Date(meal.dateOffered);
						expect(mealDate.toString()).to.equal(foundMeal.dateOffered.toString());
						expect(meal).to.haveOwnProperty("_id", foundMeal._id.toString());
						expect(meal).to.haveOwnProperty("name", foundMeal.name);
						expect(meal).to.haveOwnProperty("price", foundMeal.price);
						
						done();
					});
					
				});
		});

	});

	describe("UC304 Details of meal - GET /api/meal/:id", () => {
		it("TC-304-1 Should return valid error when meal with id doesnt exist", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/60e1c8394b9af920c8ceb3e2`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		});

		it("TC-304-2 Should return meal when meal with id exists", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					Studenthome.findOne(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const meal = res.body.meal;
						const foundMeal = home.meals[0];
						expect(meal).to.haveOwnProperty("dateCreated").to.be.a("string");
						const mealDate = new Date(meal.dateCreated);
						expect(mealDate.toString()).to.equal(foundMeal.dateCreated.toString());
						expect(meal).to.haveOwnProperty("_id", foundMeal._id.toString());
						expect(meal).to.haveOwnProperty("name", foundMeal.name);
						expect(meal).to.haveOwnProperty("price", foundMeal.price);
						done();
					});
				});
		});

	});

    
	describe("UC305 Delete meal - DELETE /api/meal/:id", () => {

		it("TC-305-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-305-2 Should return valid error when user is not the owner of the meal", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.set("Authorization", `Bearer ${fakeToken}`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "You can only delete your own meals");
				
					done();
				});
		});

		it("TC-305-3 Should return valid error when meal doesnt exist", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/60e1c8394b9af920c8ceb3e2`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		});

		it("TC-305-4 Should return valid message when meal is successfully deleted", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("deleted", "Success");
				
					done();
				});
		});
	});
    
});
