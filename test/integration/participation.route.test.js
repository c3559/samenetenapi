/* eslint-disable no-undef */
const app = require("../../app");
const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const bcrypt = require("bcrypt");
const saltrounds = parseInt(process.env.SALTROUNDS);

const Studenthome = require("../../src/models/studenthomeSchema");
const User = require("../../src/models/userSchema");

const authController = require("../../src/controllers/authentication.controller");

chai.should();
chai.use(chaiHttp);

describe("Manage participants", () => {
	let beforeEachUser;
	let beforeEachMealId;
	let beforeEachHomeId;
	let authToken;

	beforeEach(done => {
		User.create({
			firstName: "testBefore",
			lastName: "testBefore",
			studentNumber: "20984",
			email: "testBefore@gmail.com",
			password: bcrypt.hashSync("testBeforepassword", saltrounds)
		}).then(user => {
			beforeEachUser = user;
			authToken = authController.signToken(user);

			return Studenthome.create({
				maker: beforeEachUser,
				name: "testBeforeHome",
				streetname: "testBeforeHomeStreet",
				housenumber: 1,
				postalcode: "4377 OP",
				city: "testBeforeHomeCity",
				phonenumber: "0612345678"
			});
		}).then(home => {
			beforeEachHomeId = home._id;
			home.meals.push({
				maker: beforeEachUser._id,
				name: "testBeforeMeal",
				description: "testMeal",
				dateOffered: new Date(2022, 5, 3),
				price: 10,
				allergyInfo: "Test",
				ingredients: "PizzaTest",
				maxParticipants: 4
			});
			return home.save({ validateBeforeSave: true });
		}).then(homeMeal => {
			beforeEachMealId = homeMeal.meals[0]._id;
			done();
		});
	});
	describe("UC401 Sign up for meal - POST /api/studenthome/:homeId/meal/:mealId/signup", () => {
		it("TC-401-1 Should return valid error when not signed in", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/signup`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-401-2 Should return valid error when meal does not exist", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals/60e1cbae1f990e1d78653f95/signup`) 
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		});

		it("TC-401-3 Should return valid data when correctly added", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/signup`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					Studenthome.findOne(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const participant = res.body.participant;
						const foundParticipant = home.meals[0].participants[0];
						expect(participant).to.haveOwnProperty("_id", foundParticipant._id.toString());
						expect(participant).to.haveOwnProperty("user", foundParticipant.user.toString());
						done();
					});
				});
		});

	});

	describe("UC402 Sign out of meal - PUT /api/studenthome/:homeId/meal/:mealId/signoff", () => {
		it("TC-402-1 Should return valid error when not logged in", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/signoff`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-402-2 Should return valid error when meal does not exist", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/60e1cbae1f990e1d78653f95/signoff`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		});

		it("TC-402-3 Should return valid error when user was never signed up", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/signoff`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Participant not found");
				
					done();
				});
		});

		it("TC-402-4 Should return valid status when succesfully signed off", (done) => {
			//Add user to participants, then sign off
			Studenthome.findById(beforeEachHomeId).then(home => {
				home.meals.id(beforeEachMealId).participants.push({ user: beforeEachUser._id });
				return home.save();
			}).then(() => {
				chai.request(app)
					.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/signoff`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((err, res) => {
						Studenthome.findById(beforeEachHomeId).then(home => {
							expect(res).to.have.status(200);
							expect(res.body).to.haveOwnProperty("participant");
							const participants = home.meals.id(beforeEachMealId).participants;
							expect(participants).to.be.instanceOf(Array).and.to.have.lengthOf(0);

							done();
						});
					});
					
			});
			
		}); 

	});


	describe("UC403 Overview of participants - GET /api/meal/:mealId/participants", () => {
		it("TC-403-1 Should return valid error when not logged in", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/participants`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-403-2 Should return valid error when meal does not exist", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/60e1cbae1f990e1d78653f95/participants`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		});

		it("TC-403-3 Should return list of participants", (done) => {
			//Add user to participants, then return list
			Studenthome.findById(beforeEachHomeId).then(home => {
				home.meals.id(beforeEachMealId).participants.push({ user: beforeEachUser._id });
				return home.save();
			}).then(() => {		
				chai.request(app)
					.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/participants`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((err, res) => {
						Studenthome.findOne(beforeEachHomeId).then(home => {
							expect(res).to.have.status(200);
							const foundParticipant = home.meals[0].participants[0];
							expect(res.body.participants).to.be.instanceOf(Array).and.to.have.lengthOf(0);
							const participant = res.body.participants[0];
							expect(participant).to.haveOwnProperty("signupDate").to.be.a("string");
							const participantDate = new Date(meal.dateCreated);
							expect(participantDate.toString()).to.equal(foundParticipant.signupDate.toString());
							expect(participant).to.haveOwnProperty("_id", foundParticipant._id.toString());
							expect(participant).to.haveOwnProperty("user", foundParticipant.user.toString());
							done();
						});
						

						done();
					});
			});
		});

	});

	describe("UC404 Details of participant - GET /api/meal/:mealId/participants/:participantId", () => {
		it("TC-404-1 Should return valid error when user not logged in", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/participants/${beforeEachUser._id}`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-404-2 Should return valid error when participant does not exist", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/participants/${beforeEachUser._id}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Participant not found");
				
					done();
				});
		});

		it("TC-404-2 Should return valid details of participant", (done) => {
			//Add user to participants, then return participant
			Studenthome.findById(beforeEachHomeId).then(home => {
				home.meals.id(beforeEachMealId).participants.push({ user: beforeEachUser._id });
				return home.save();
			}).then(() => {		
				chai.request(app)
					.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/participants/${beforeEachUser._id}`)
					.set("Authorization", `Bearer ${authToken}`)
					.end((err, res) => {
						Studenthome.findOne(beforeEachHomeId).then(home => {
							expect(res).to.have.status(200);
							const foundParticipant = home.meals[0].participants[0];
							const participant = res.body.participant;
							expect(participant).to.haveOwnProperty("signupDate").to.be.a("string");
							const participantDate = new Date(meal.dateCreated);
							expect(participantDate.toString()).to.equal(foundParticipant.signupDate.toString());
							expect(participant).to.haveOwnProperty("_id", foundParticipant._id.toString());
							expect(participant).to.haveOwnProperty("user", foundParticipant.user.toString());
							done();
						});
						

						done();
					});
			});
		});

	});
});
