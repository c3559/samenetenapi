/* eslint-disable no-undef */
const app = require("../../app");

const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const bcrypt = require("bcrypt");
const saltrounds = parseInt(process.env.SALTROUNDS);

const Studenthome = require("../../src/models/studenthomeSchema");
const User = require("../../src/models/userSchema");

const authController = require("../../src/controllers/authentication.controller");

chai.should();
chai.use(chaiHttp);
	

//review Tests
describe("Manage reviews", () => {
	let beforeEachUser;
	let beforeEachReviewId;
	let beforeEachMealId;
	let beforeEachHomeId;
	let authToken;
	let fakeToken;

	beforeEach(done => {
		User.create({
			firstName: "testFakeBefore",
			lastName: "testFakeBefore",
			studentNumber: "20984",
			email: "testFakeBefore@gmail.com",
			password: bcrypt.hashSync("testFakeBeforepassword", saltrounds)
		}).then(user => {
			fakeToken = authController.signToken(user);
			return User.create({
				firstName: "testBefore",
				lastName: "testBefore",
				studentNumber: "20984",
				email: "testBefore@gmail.com",
				password: bcrypt.hashSync("testBeforepassword", saltrounds)
			});
		}).then(user =>  {
			beforeEachUser = user;
			authToken = authController.signToken(user);
			return Studenthome.create({
				maker: beforeEachUser,
				name: "testBeforeHome",
				streetname: "testBeforeHomeStreet",
				housenumber: 1,
				postalcode: "4377 OP",
				city: "testBeforeHomeCity",
				phonenumber: "0612345678"
			});
		}).then(home => {
			beforeEachHomeId = home._id;
			home.meals.push({
				maker: beforeEachUser._id,
				name: "testBeforeMeal",
				description: "testMeal",
				dateOffered: new Date(2022, 5, 3),
				price: 10,
				allergyInfo: "Test",
				ingredients: "PizzaTest",
				maxParticipants: 4
			});
			return home.save({ validateBeforeSave: true });
		}).then(home => {
			beforeEachMealId = home.meals[0]._id;
			home.meals[0].reviews.push({
				"maker": beforeEachUser,
				"title": "testTitle",
				"description": "testDescription",
				"rating": 10,
				"recommended": true
			});
			return home.save({ validateBeforeSave: true });
		}).then(homeReview => {
			beforeEachReviewId = homeReview.meals[0].reviews[0]._id;
			done();
		}).catch(error => {
			console.log(error);
		});
	});

	describe("UC501 Create review - POST /api/reviews", () => {
		it("TC-501-1 Should return valid error when required field is not present", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					title: "testTitle",
					//description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "studenthomes validation failed: meals.0.reviews.1.description: Review description is required");
				
					done();
				});
		});

		it("TC-501-2 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-501-3 Should return valid review when correctly added", (done) => {
			chai.request(app)
				.post(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					Studenthome.findById(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const review = res.body.review;
						const foundReview = home.meals[0].reviews[1];
						expect(review).to.haveOwnProperty("_id", foundReview._id.toString());
						expect(review).to.haveOwnProperty("title", foundReview.title);
						expect(review).to.haveOwnProperty("rating", foundReview.rating);
						done();
					});
					
				});
		});

	});

	describe("UC502 Edit review - PUT /api/reviews/:id", () => {
		
		it("TC-502-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-502-2 Should return valid error when user is not owner of review", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${fakeToken}`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "You can only edit your own reviews");
				
					done();
				});
		});

		it("TC-502-3 Should return valid error when meal doesnt exist", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/60e1c8394b9af920c8ceb3e2/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		}); 

		it("TC-502-4 Should return valid error when review doesnt exist", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/60e1c8394b9af920c8ceb3e2`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Review not found");
				
					done();
				});
		}); 

		it("TC-502-5 Should return valid review when correctly edited", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					title: "testTitle",
					description: "testDescription",
					rating: 10,
					recommended: true
				})
				.end((err, res) => {
					Studenthome.findById(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const meal = res.body.updatedMeal;
						const review = meal.reviews[0];
						const foundReview = home.meals[0].reviews[0];
						expect(review).to.haveOwnProperty("_id", foundReview._id.toString());
						expect(review).to.haveOwnProperty("title", foundReview.title);
						expect(review).to.haveOwnProperty("rating", foundReview.rating);
						done();
					});
				});
		});

	});


	describe("UC503 Overview of reviews - GET /api/reviews/", () => {
		it("TC-503-1 Should return all reviews", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					Studenthome.findOne(beforeEachHomeId).then(home => {
						foundReviews = home.meals[0].reviews;
						review = res.body.reviews[0];
						foundReview = foundReviews[0];
						expect(review).to.haveOwnProperty("_id", foundReview._id.toString());
						expect(review).to.haveOwnProperty("title", foundReview.title);
						expect(review).to.haveOwnProperty("rating", foundReview.rating);
						
						done();
					});
					
				});
		});

	});

	describe("UC504 Details of review - GET /api/reviews/:id", () => {
		it("TC-504-1 Should return valid error when review with id doesnt exist", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/60e1c8394b9af920c8ceb3e2`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Review not found");
				
					done();
				});
		});

		it("TC-504-2 Should return review when review with id exists", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					Studenthome.findOne(beforeEachHomeId).then(home => {
						expect(res).to.have.status(200);
						const review = res.body.review;
						const foundReview = home.meals[0].reviews[0];
						expect(review).to.haveOwnProperty("_id", foundReview._id.toString());
						expect(review).to.haveOwnProperty("title", foundReview.title);
						expect(review).to.haveOwnProperty("rating", foundReview.rating);
						done();
					});
				});
		});

	});

    
	describe("UC505 Delete review - DELETE /api/reviews/:id", () => {

		it("TC-505-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-505-2 Should return valid error when user is not the owner of the review", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${fakeToken}`)
				.end((err, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "You can only delete your own reviews");
				
					done();
				});
		});

		it("TC-505-3 Should return valid error when meal doesnt exist", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/60e1c8394b9af920c8ceb3e2/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Meal not found");
				
					done();
				});
		});

		it("TC-505-3 Should return valid error when review doesnt exist", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/60e1c8394b9af920c8ceb3e2`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Review not found");
				
					done();
				});
		});

		it("TC-505-4 Should return valid message when review is successfully deleted", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}/meals/${beforeEachMealId}/reviews/${beforeEachReviewId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("deleted", "Success");
				
					done();
				});
		});
	});
    
});
