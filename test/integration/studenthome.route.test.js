/* eslint-disable no-undef */
const app = require("../../app");

const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");
const bcrypt = require("bcrypt");
const saltrounds = parseInt(process.env.SALTROUNDS);

const Studenthome = require("../../src/models/studenthomeSchema");
const User = require("../../src/models/userSchema");

const authController = require("../../src/controllers/authentication.controller");

chai.should();
chai.use(chaiHttp);


//Studenthome Tests
describe("Manage studenthomes", () => {
	let beforeEachUser;
	let beforeEachHomeId;
	let authToken;
	let fakeUser;

	beforeEach(done => {
		User.create({
			firstName: "testFakeBefore",
			lastName: "testFakeBefore",
			studentNumber: "20984",
			email: "testFakeBefore@gmail.com",
			password: bcrypt.hashSync("testFakeBeforepassword", saltrounds)
		}).then(user => {
			fakeUser = user;
			return User.create({
				firstName: "testBefore",
				lastName: "testBefore",
				studentNumber: "20984",
				email: "testBefore@gmail.com",
				password: bcrypt.hashSync("testBeforepassword", saltrounds)
			});
		}).then(user =>  {
			beforeEachUser = user;
			authToken = authController.signToken(user);

			return Studenthome.create({
				maker: beforeEachUser,
				name: "testBeforeHome",
				streetname: "testBeforeHomeStreet",
				housenumber: 1,
				postalcode: "4377 OP",
				city: "testBeforeHomeCity",
				phonenumber: "0612345678"
			});
		}).then(() => {
			return Studenthome.create({
				maker: beforeEachUser,
				name: "testBeforeHome2",
				streetname: "testBeforeHomeStreet2",
				housenumber: 2,
				postalcode: "4377 OP",
				city: "testBeforeHomeCity2",
				phonenumber: "0612345678"
			});
		}).then(home => {
			beforeEachHomeId = home._id;
			done();
		});
	});
	describe("UC201 Create studenthome - POST /api/studenthome", () => {
		it("TC-201-1 Should return valid error when required field is not present", (done) => {
			chai.request(app)
				.post("/api/studenthomes")
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					//housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "studenthomes validation failed: housenumber: Studenthome housenumber is required");
				
					done();
				});
		});

		it("TC-201-2 Should return valid error when postal code is not valid", (done) => {
			chai.request(app)
				.post("/api/studenthomes")
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "10000F",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "studenthomes validation failed: postalcode: Path `postalcode` is invalid (10000F).");
				
					done();
				});
		});

		it("TC-201-3 Should return valid error when phone number is not valid", (done) => {
			chai.request(app)
				.post("/api/studenthomes")
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "1234"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "studenthomes validation failed: phonenumber: Path `phonenumber` is invalid (1234).");
				
					done();
				});
		});

		it("TC-201-4 Should return valid error when studenthome is already made", (done) => {
			chai.request(app)
				.post("/api/studenthomes")
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testBeforeHome",
					streetname: "testBeforeHomeStreet",
					housenumber: 1,
					postalcode: "4377 OP",
					city: "testBeforeHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Studenthome already exists");
				
					done();
				});
		});

		it("TC-201-5 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.post("/api/studenthomes")
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-201-6 Should return valid studenthome when correctly added", (done) => {
			chai.request(app)
				.post("/api/studenthomes")
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "0612345678" 
				})
				.end((_, res) => {
					Studenthome.findOne({ name: "testHome" }).then(studenthome => {
						expect(res).to.have.status(200);
						const home = res.body.home;
						
						expect(home).to.haveOwnProperty("_id", studenthome._id.toString());
						expect(home).to.haveOwnProperty("name", studenthome.name);
						expect(home).to.haveOwnProperty("postalcode", studenthome.postalcode);
						expect(home).to.haveOwnProperty("housenumber", studenthome.housenumber);

						done();
					});
				});
		});

	});

	describe("UC202 Overview of studenthomes - GET /api/studenthome", () => {
		it("TC-202-1 Should return 2 studenthomes", (done) => {
			chai.request(app)
				.get("/api/studenthomes?name=test")
				.end((_, res) => {
					Studenthome.find({ name: /^test/ }).then(homes => {
						
						expect(res).to.have.status(200);
						const studenthomes = res.body.studenthomes;
						const studenthome1 = studenthomes[0];
						const studenthome2 = studenthomes[1];

						const home1 = homes[0];
						const home2 = homes[1];

						expect(studenthome1).to.haveOwnProperty("_id", home1._id.toString());
						expect(studenthome1).to.haveOwnProperty("name", home1.name);
						expect(studenthome1).to.haveOwnProperty("city", home1.city);
						expect(studenthome2).to.haveOwnProperty("_id", home2._id.toString());
						expect(studenthome2).to.haveOwnProperty("name", home2.name);
						expect(studenthome2).to.haveOwnProperty("city", home2.city);

						done();
					});
				});
		});

		it("TC-202-2 Should return valid error when searching for non-existent city", (done) => {
			chai.request(app)
				.get("/api/studenthomes?city=noexist")
				.end((_, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "No studenthomes found");
				
					done();
				});
		});

		it("TC-202-3 Should return valid error when searching for non-existent name", (done) => {
			chai.request(app)
				.get("/api/studenthomes?name=noexist")
				.end((_, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "No studenthomes found");
				
					done();
				});
		});

		it("TC-202-4 Should return studenthomes when searching for existing city", (done) => {
			chai.request(app)
				.get("/api/studenthomes?city=testBeforeHomeCity")
				.end((_, res) => {
					Studenthome.find({ city: "testBeforeHomeCity" }).then(homes => {
						expect(res).to.have.status(200);
						const studenthomes = res.body.studenthomes;
						const studenthome1 = studenthomes[0];
						const home1 = homes[0];

						expect(studenthome1).to.haveOwnProperty("_id", home1._id.toString());
						expect(studenthome1).to.haveOwnProperty("name", home1.name);
						expect(studenthome1).to.haveOwnProperty("city", home1.city);

						done();
					});
				});
		});

		it("TC-202-5 Should return studenthomes when searching for existing name", (done) => {
			chai.request(app)
				.get("/api/studenthomes?name=testBeforeHome2")
				.end((_, res) => {
					Studenthome.find({ name: "testBeforeHome2" }).then(homes => {
						expect(res).to.have.status(200);
						const studenthomes = res.body.studenthomes;
						const studenthome1 = studenthomes[0];
						const home1 = homes[0];

						expect(studenthome1).to.haveOwnProperty("_id", home1._id.toString());
						expect(studenthome1).to.haveOwnProperty("name", home1.name);
						expect(studenthome1).to.haveOwnProperty("city", home1.city);

						done();
					});
				});
		});
	});

	describe("UC203 Details of studenthome - GET /api/studenthome/:id", () => {
		it("TC-203-1 Should return valid error when studenthome with id doesnt exist", (done) => {
			const fakeHomeId = "60e08fb7e5b61a42287ec0cf";
			chai.request(app)
				.get(`/api/studenthomes/${fakeHomeId}`)
				.end((_, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Studenthome not found");
				
					done();
				});
		});

		it("TC-203-2 Should return studenthome when studenthome with id exists", (done) => {
			chai.request(app)
				.get(`/api/studenthomes/${beforeEachHomeId}`)
				.end((_, res) => {
					Studenthome.findOne({ name: "testBeforeHome2" }).then(home => {
						expect(res).to.have.status(200);
						const studenthome = res.body.studenthome;
						
						expect(studenthome).to.haveOwnProperty("_id", home._id.toString());
						expect(studenthome).to.haveOwnProperty("name", home.name);
						expect(studenthome).to.haveOwnProperty("postalcode", home.postalcode);
						expect(studenthome).to.haveOwnProperty("housenumber", home.housenumber);
				
						done();
					});
					
				});
		});

	});

	describe("UC204 Edit studenthome - PUT /api/studenthome/:id", () => {

		it("TC-204-1 Should return valid error when postal code is not valid", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "10000",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Validation failed: postalcode: Path `postalcode` is invalid (10000).");
				
					done();
				});
		});

		it("TC-204-2 Should return valid error when phone number is not valid", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "1234"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Validation failed: phonenumber: Path `phonenumber` is invalid (1234).");
				
					done();
				});
		});

		it("TC-204-3 Should return valid error when studenthome doesnt exist", (done) => {
			const fakeHomeId = "60e08fb7e5b61a42287ec0cf";
			chai.request(app)
				.put(`/api/studenthomes/${fakeHomeId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(400);
					expect(res.body).to.haveOwnProperty("error", "Studenthome not found");
				
					done();
				});

				
		});

		it("TC-204-4 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
					done();
				});
		});

		it("TC-204-5 Should return valid studenthome when correctly edited", (done) => {
			chai.request(app)
				.put(`/api/studenthomes/${beforeEachHomeId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({
					name: "testHome",
					streetname: "testHomeStreet",
					housenumber: 40,
					postalcode: "9274 GF",
					city: "testHomeCity",
					phonenumber: "0612345678"
				})
				.end((_, res) => {
					Studenthome.findOne({ name: "testHome" }).then(home => {
						expect(res).to.have.status(200);
						const studenthome = res.body.updatedHome;

						expect(studenthome).to.haveOwnProperty("_id", home._id.toString());
						expect(studenthome).to.haveOwnProperty("name", home.name);
						expect(studenthome).to.haveOwnProperty("postalcode", home.postalcode);
						expect(studenthome).to.haveOwnProperty("housenumber", home.housenumber);

						done();
					});
				});
		});

	});

	describe("UC205 Delete studenthome - DELETE /api/studenthome/:id", () => {
		it("TC-205-1 Should return valid error when studenthome doesnt exist", (done) => {
			const fakeHomeId = "60e08fb7e5b61a42287ec0cf";
			chai.request(app)
				.delete(`/api/studenthomes/${fakeHomeId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "Studenthome not found");
				
					done();
				});
		});

	
		it("TC-205-2 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}`)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});
	
		it("TC-205-3 Should return valid error when user is not the owner of the studenthome", (done) => {
			const fakeToken = authController.signToken(fakeUser);
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}`)
				.set("Authorization", `Bearer ${fakeToken}`)
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "You can only delete your own Studenthome");
				
					done();
				});
		});

		it("TC-205-4 Should return valid message when studenthome is successfully deleted", (done) => {
			chai.request(app)
				.delete(`/api/studenthomes/${beforeEachHomeId}`)
				.set("Authorization", `Bearer ${authToken}`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("Delete", "Success");
				
					done();
				});
		});
	});

});
