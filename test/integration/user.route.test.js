/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const bcrypt = require("bcrypt");
const app = require("../../app");
const User = require("../../src/models/userSchema");
const Studenthome = require("../../src/models/studenthomeSchema");
require("dotenv").config();
const authController = require("../../src/controllers/authentication.controller");

const neo4j = require("neo4j-driver");
const driver = neo4j.driver(process.env.NEO4J_CONNECTION_URL, neo4j.auth.basic("neo4j", process.env.NEO4J_CONNECTION_PASSWORD));
const chai = require("chai");
const expect = chai.expect;
const chaiHttp = require("chai-http");


chai.should();
chai.use(chaiHttp);

const saltrounds = parseInt(process.env.SALTROUNDS);

describe("Manage users", () => {
	let user1;
	let user2;
	let authToken;
	let beforeEachHomeId;
	let beforeEachMeal;
	
	beforeEach(done => {
		const session = driver.session();
		User.create({
			firstName: "neo4jtestuserfirstnamebeforeeach",
			lastName: "test",
			studentNumber: "1",
			email: "test@gmail.com",
			password: bcrypt.hashSync("testpassword", saltrounds)
		}).then(user =>  {
			user1 = user;
			authToken = authController.signToken(user);
			return User.create({
				firstName: "neo4jtestuserfirstnamebeforeeach2",
				lastName: "test2",
				studentNumber: "2",
				email: "test2@gmail.com",
				password: bcrypt.hashSync("testpassword2", saltrounds)
			});
		}).then(user => {
			user2 = user;
			return Studenthome.create({
				maker: user2,
				name: "testBeforeHome",
				streetname: "testBeforeHomeStreet",
				housenumber: 1,
				postalcode: "4377 OP",
				city: "testBeforeHomeCity",
				phonenumber: "0612345678"
			});
		}).then(home => {
			beforeEachHomeId = home._id;
			home.meals.push({
				maker: user2._id,
				name: "testBeforeMeal",
				description: "testMeal",
				dateOffered: new Date(2022, 5, 3),
				price: 10,
				allergyInfo: "Test",
				ingredients: "PizzaTest",
				maxParticipants: 4
			});
			return home.save({ validateBeforeSave: true });
		}).then(homeMeal => {
			beforeEachMeal = homeMeal.meals[0];
		}).then(() => {
			return session.run(`CREATE (user:User {_id: "${user1._id}", firstName: "${user1.firstName}", lastName: "${user1.lastName}", email: "${user1.email}"})`);
		}).then(() => {
			return session.run(`CREATE (user:User {_id: "${user2._id}", firstName: "${user2.firstName}", lastName: "${user2.lastName}", email: "${user2.email}"})`);
		}).then(() => {
			session.close();
			done();
		});
	});


	describe("UC-601 Follow user - POST /api/users/:userId/follow", () => {

		it("TC-601-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.post(`/api/users/${user2._id}/follow`)
				.send({ })
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});
		it("TC-601-2 Should return valid error when user not found", (done) => {
			chai.request(app)
				.post("/api/users/60e08fb7e5b61a42287ec0cf/follow")
				.set("Authorization", `Bearer ${authToken}`)
				.send({	})
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "User not found");
				
					done();
				});
		});

		it("TC-601-3 Should return success message when user is succesfully followed", (done) => {
			chai.request(app)
				.post(`/api/users/${user2._id}/follow`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({	})
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("success", "You are now following!");

					done();
				});
		});

	});

	describe("UC-602 Unfollow - POST /api/users/:userId/follow", () => {
		it("TC-602-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.post(`/api/users/${user2._id}/unfollow`)
				.send({ })
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-602-2 Should return valid error when user not found", (done) => {
			chai.request(app)
				.post("/api/users/60e08fb7e5b61a42287ec0cf/unfollow")
				.set("Authorization", `Bearer ${authToken}`)
				.send({	})
				.end((err, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "User not found");
				
					done();
				});
		});

		it("TC-602-3 Should return success message when user is succesfully unfollowed", (done) => {
			chai.request(app)
				.post(`/api/users/${user2._id}/unfollow`)
				.set("Authorization", `Bearer ${authToken}`)
				.send({	})
				.end((err, res) => {
					expect(res).to.have.status(200);
					expect(res.body).to.haveOwnProperty("success", "You have unfollowed!");

					done();
				});
		});
	});

	describe("UC-603 Get followed users - GET /api/users/following", () => {
		it("TC-603-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.get("/api/users/following")
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-603-2 Should return list of users", (done) => {
			const session = driver.session();
			session.run(`MATCH (a:User) WHERE a._id = "${user1._id}" MATCH (b:User) WHERE b._id = "${user2._id}" CREATE (a)-[r:FOLLOWS]->(b)`).then(() => {
				chai.request(app)
					.get("/api/users/following")
					.set("Authorization", `Bearer ${authToken}`)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("users");
						const bodyusers = res.body.users;
						const bodyuser1 = bodyusers[0];
						expect(bodyuser1).to.haveOwnProperty("_id", user2._id.toString());
						expect(bodyuser1).to.haveOwnProperty("firstName", user2.firstName);
						expect(bodyuser1).to.haveOwnProperty("email", user2.email);
						done();
					});
			}).then(() => {
				session.close();
			});
			
			
		});

	});

	
	describe("UC604 Details of user - GET /api/users/:userId", () => {
		it("TC-604-1 Should return valid error when user with id doesnt exist", (done) => {
			chai.request(app)
				.get("/api/users/60e08fb7e5b61a42287ec0cf")
				.end((_, res) => {
					expect(res).to.have.status(404);
					expect(res.body).to.haveOwnProperty("error", "User not found");
				
					done();
				});
		});

		it("TC-604-2 Should return user when user with id exists", (done) => {
			chai.request(app)
				.get(`/api/users/${user1._id}`)
				.end((_, res) => {
					expect(res).to.have.status(200);
					const bodyuser1 = res.body.user;
					expect(bodyuser1).to.haveOwnProperty("_id", user1._id.toString());
					expect(bodyuser1).to.haveOwnProperty("firstName", user1.firstName);
					expect(bodyuser1).to.haveOwnProperty("email", user1.email);
					done();
				});
		});

	});



	describe("UC-605 Get followed users - GET /api/users/following/meals", () => {
		it("TC-605-1 Should return valid error when user is not logged in", (done) => {
			chai.request(app)
				.get("/api/users/following/meals")
				.end((_, res) => {
					expect(res).to.have.status(401);
					expect(res.body).to.haveOwnProperty("error", "Not authenticated");
				
					done();
				});
		});

		it("TC-605-2 Should return list of meals", (done) => {
			const session = driver.session();
			session.run(`MATCH (a:User) WHERE a._id = "${user1._id}" MATCH (b:User) WHERE b._id = "${user2._id}" CREATE (a)-[r:FOLLOWS]->(b)`).then(() => {
				chai.request(app)
					.get("/api/users/following/meals")
					.set("Authorization", `Bearer ${authToken}`)
					.end((err, res) => {
						expect(res).to.have.status(200);
						expect(res.body).to.haveOwnProperty("meals");
						const bodymeals = res.body.meals;
						const bodymeal1 = bodymeals[0];
						expect(bodymeal1).to.haveOwnProperty("_id", beforeEachMeal._id.toString());
						expect(bodymeal1).to.haveOwnProperty("name", beforeEachMeal.name);
						expect(bodymeal1).to.haveOwnProperty("price", beforeEachMeal.price);
						done();
					});
			}).then(() => {
				session.close();
			});
			
			
		});

	});

	afterEach(done => {
		const session = driver.session();
		session.run(`MATCH (a:User) WHERE a.firstName = "${user1.firstName}" MATCH (b:User) WHERE b.firstName = "${user2.firstName}"  MATCH (a)-[r:FOLLOWS]->(b) DELETE r`).then(() => {
			return session.run(`MATCH (a:User) WHERE a.firstName = "${user1.firstName}" DELETE a`);
		}).then(() => {
			return session.run(`MATCH (a:User) WHERE a.firstName = "${user2.firstName}" DELETE a`);
		}).then(() => {
			session.close();
			done();
		});
		
	});

});

