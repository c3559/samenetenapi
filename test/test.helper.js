/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const mongoose = require("mongoose");

const User = require("../src/models/userSchema");
const Studenthome = require("../src/models/studenthomeSchema");

before(done => {
	require("tracer").setLevel("warn");

	mongoose.connect(
		`${process.env.MONGODB_CONNECTIONTEST_URL}?retryWrites=true&w=majority`,
		{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false }
	);

	mongoose.connection
		.once("open", () => done());
});

beforeEach(async() => {
	await Promise.all([User.deleteMany(),Studenthome.deleteMany()]);	
});